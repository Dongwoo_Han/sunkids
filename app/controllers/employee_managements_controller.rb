class EmployeeManagementsController < ApplicationController
  before_action :set_employee_management, only: [:show, :edit, :update, :destroy]

  # GET /employee_managements
  # GET /employee_managements.json
  def index
    @employee_managements = EmployeeManagement.all
  end

  # GET /employee_managements/1
  # GET /employee_managements/1.json
  def show
  end

  # GET /employee_managements/new
  def new
    @employee_management = EmployeeManagement.new
  end

  # GET /employee_managements/1/edit
  def edit
  end

  # POST /employee_managements
  # POST /employee_managements.json
  def create
    @employee_management = EmployeeManagement.new(employee_management_params)

    respond_to do |format|
      if @employee_management.save
        format.html { redirect_to @employee_management, notice: 'Employee management was successfully created.' }
        format.json { render :show, status: :created, location: @employee_management }
      else
        format.html { render :new }
        format.json { render json: @employee_management.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /employee_managements/1
  # PATCH/PUT /employee_managements/1.json
  def update
    respond_to do |format|
      if @employee_management.update(employee_management_params)
        format.html { redirect_to @employee_management, notice: 'Employee management was successfully updated.' }
        format.json { render :show, status: :ok, location: @employee_management }
      else
        format.html { render :edit }
        format.json { render json: @employee_management.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /employee_managements/1
  # DELETE /employee_managements/1.json
  def destroy
    @employee_management.destroy
    respond_to do |format|
      format.html { redirect_to employee_managements_url, notice: 'Employee management was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_employee_management
      @employee_management = EmployeeManagement.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def employee_management_params
      params.require(:employee_management).permit(:employee, :clockin, :clockout)
    end
end
