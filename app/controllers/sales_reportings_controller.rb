class SalesReportingsController < ApplicationController
  before_action :set_sales_reporting, only: [:show, :edit, :update, :destroy]

  # GET /sales_reportings
  # GET /sales_reportings.json
  def index
    @sales_reportings = SalesReporting.all
  end

  # GET /sales_reportings/1
  # GET /sales_reportings/1.json
  def show
  end

  # GET /sales_reportings/new
  def new
    @sales_reporting = SalesReporting.new
  end

  # GET /sales_reportings/1/edit
  def edit
  end

  # POST /sales_reportings
  # POST /sales_reportings.json
  def create
    @sales_reporting = SalesReporting.new(sales_reporting_params)

    respond_to do |format|
      if @sales_reporting.save
        format.html { redirect_to @sales_reporting, notice: 'Sales reporting was successfully created.' }
        format.json { render :show, status: :created, location: @sales_reporting }
      else
        format.html { render :new }
        format.json { render json: @sales_reporting.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /sales_reportings/1
  # PATCH/PUT /sales_reportings/1.json
  def update
    respond_to do |format|
      if @sales_reporting.update(sales_reporting_params)
        format.html { redirect_to @sales_reporting, notice: 'Sales reporting was successfully updated.' }
        format.json { render :show, status: :ok, location: @sales_reporting }
      else
        format.html { render :edit }
        format.json { render json: @sales_reporting.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /sales_reportings/1
  # DELETE /sales_reportings/1.json
  def destroy
    @sales_reporting.destroy
    respond_to do |format|
      format.html { redirect_to sales_reportings_url, notice: 'Sales reporting was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_sales_reporting
      @sales_reporting = SalesReporting.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def sales_reporting_params
      params.require(:sales_reporting).permit(:product, :quantity, :amount_paid)
    end
end
