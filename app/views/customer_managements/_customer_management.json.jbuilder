json.extract! customer_management, :id, :customer, :amount_paid, :created_at, :updated_at
json.url customer_management_url(customer_management, format: :json)
