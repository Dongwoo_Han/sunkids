json.extract! employee_management, :id, :employee, :clockin, :clockout, :created_at, :updated_at
json.url employee_management_url(employee_management, format: :json)
