json.extract! product, :id, :product, :quantity, :unit_purchasing_price, :unit_selling_price, :supplier, :created_at, :updated_at
json.url product_url(product, format: :json)
