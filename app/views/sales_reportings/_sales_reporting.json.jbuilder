json.extract! sales_reporting, :id, :product, :quantity, :amount_paid, :created_at, :updated_at
json.url sales_reporting_url(sales_reporting, format: :json)
