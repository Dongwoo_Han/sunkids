json.extract! inventory, :id, :title, :created_at, :updated_at
json.url inventory_url(inventory, format: :json)
