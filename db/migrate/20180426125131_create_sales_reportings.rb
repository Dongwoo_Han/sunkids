class CreateSalesReportings < ActiveRecord::Migration[5.1]
  def change
    create_table :sales_reportings do |t|
      t.string :product
      t.integer :quantity
      t.float :amount_paid

      t.timestamps
    end
  end
end
