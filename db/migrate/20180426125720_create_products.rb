class CreateProducts < ActiveRecord::Migration[5.1]
  def change
    create_table :products do |t|
      t.string :product
      t.integer :quantity
      t.float :unit_purchasing_price
      t.float :unit_selling_price
      t.string :supplier

      t.timestamps
    end
  end
end
