class CreatePsots < ActiveRecord::Migration[5.1]
  def change
    create_table :psots do |t|
      t.string :anem
      t.string :title
      t.text :content

      t.timestamps
    end
  end
end
