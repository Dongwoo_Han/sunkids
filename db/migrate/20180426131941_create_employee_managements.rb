class CreateEmployeeManagements < ActiveRecord::Migration[5.1]
  def change
    create_table :employee_managements do |t|
      t.string :employee
      t.float :clockin
      t.float :clockout

      t.timestamps
    end
  end
end
