class CreateCustomerManagements < ActiveRecord::Migration[5.1]
  def change
    create_table :customer_managements do |t|
      t.string :customer
      t.float :amount_paid

      t.timestamps
    end
  end
end
