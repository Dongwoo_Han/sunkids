Feature: 
    This Point-of-Sale lets a retailer keep track of their sales and inventory, and manage their customers and employees.

Background:
    Given the market has 10 products in stock
    Given the market has sold 5 products
    Given the market has 5 customer visits
    Given the market has 3 employees



1
Scenario: User navigates to Sales Reportings page and records a product after it is sold.
    Given the user logs in
    Then the user should see the homepage of the software
    When the user clicks "Sales Reporting"
    Then the user should see the homepage of Sales Reportings with the 5 products that are sold
    When the user clicks "New Sales Reporting" 
    Then the user enters "Coffee" for product name
    Then the user enters "1" for quantity
    Then the user enters "2000" for amount paid
    When the user clicks "Create Sales reporting"
    Then the user should see the message "Sales reporting was successfully created" with the data that is just recorded


2
Scenario: User navigates to Sales Reportings page and destroys an item that is recorded as sale
    Given the user logs in
    Then the user should see the homepage of the software
    When the user clicks "Sales Reporting"
    Then the user should see the homepage of Sales Reportings with the 5 products that are sold
    When the user clicks "Destroy" for Coca Cola
    Then the user gets "Are you sure?" message
    When the user clicks "OK"
    Then the user should see an updated list with just 4 items
   
   
3
Scenario: User navigates to Sales Reportings page and edits an item that is recorded as sale by changing its quantity
    Given the user logs in
    Then the user should see the homepage of the software
    When the user clicks "Sales Reporting"
    Then the user should see the homepage of Sales Reportings with the 5 products that are sold
    When the user clicks "Edit" for Coca Cola
    Then the user changes the quantity to "2"
    When the user clicks "Update Sales reporting"
    Then the user should see a confirmation message with updated information
    
    
4
Scenario: User navigates to Product page and records a new inventory
    Given the user logs in
    Then the user should see the homepage of the software
    When the user clicks "Product"
    Then the user should see the homepage of Product with the 10 products in stock
    When the user clicks "New Product" 
    Then the user enters "Oreo" for product name
    Then the user enters "1" for quantity
    Then the user enters "1500" for price
    When the user clicks "Create Product"
    Then the user should see the message "Sales reporting was successfully created" with the data that is just recorded
    

5
Scenario: User navigates to Product page and destroys an item
    Given the user logs in
    Then the user should see the homepage of the software
    When the user clicks "Product"
    Then the user should see the homepage of Product with the 10 products in stock
    When the user clicks "Destroy" for Notepad
    Then the user gets "Are you sure?" message
    When the user clicks "OK"
    Then the user should see an updated list with just 9 items
    
    
6
Scenario: User navigates to Product page and edits an item by increasing its selling price
    Given the user logs in
    Then the user should see the homepage of the software
    When the user clicks "Product"
    Then the user should see the homepage of Product with the 10 products in stock
    When the user clicks "Edit" for Notepad
    Then the user changes the Unit selling price to "1700"
    When the user clicks "Update Product"
    Then the user should see a confirmation message with updated information


7
Scenario: User navigates to Customer Management page and adds information about a new customer
    Given the user logs in
    Then the user should see the homepage of the software
    When the user clicks "Customer Management"
    Then the user should see the homepage of Customer Management with a list of the 5 customers
    When the user clicks "New Customer Management" 
    Then the user enters "Youjin" for customer
    Then the user enters "18500" for amount paid
    When the user clicks "Create Customer Management"
    Then the user should see the message "Customer management was successfully created" with the data that is just recorded


8
Scenario: User navigates to Customer Management page and updates customer information by increasing the amount paid
    Given the user logs in
    Then the user should see the homepage of the software
    When the user clicks "Customer Management"
    Then the user should see the homepage of Customer Management with a list of the 5 customers
    When the user clicks "Edit" for Jiyeon 
    Then the user changes amount paid to "45000"
    When the user clicks "Update Customer management"
    Then the user should see a confirmation message with updated information


9
Scenario: User navigates to Employee Management page and adds new information about a new employee
    Given the user logs in
    Then the user should see the homepage of the software
    When the user clicks "Employee Management"
    Then the user should see the homepage of Employee Management with a list of the 3 customers
    When the user clicks "New Employee Management" 
    Then the user enters "Eunchae" for employee
    Then the user enters "11" for clockin
    Then the user enters "21" for clockout
    When the user clicks "Create Employee Management"
    Then the user should see the message "Employee management was successfully created" with the data that is just recorded


10
Scenario: User navigates to Employee Management page and deletes employee informatino after the wage payment is completed at the end of the month
    Given the user logs in
    Then the user should see the homepage of the software
    When the user clicks "Employee Management"
    Then the user should see the homepage of Employee Management with a list of the 3 customers
    When the user clicks "Destroy" for Nara
    Then the user gets an "Are you sure?" message
    When the user clicks "OK"
    Then the user should see a confirmation message with the uppated employees list


