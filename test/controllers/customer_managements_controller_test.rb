require 'test_helper'

class CustomerManagementsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @customer_management = customer_managements(:one)
  end

  test "should get index" do
    get customer_managements_url
    assert_response :success
  end

  test "should get new" do
    get new_customer_management_url
    assert_response :success
  end

  test "should create customer_management" do
    assert_difference('CustomerManagement.count') do
      post customer_managements_url, params: { customer_management: { amount_paid: @customer_management.amount_paid, customer: @customer_management.customer } }
    end

    assert_redirected_to customer_management_url(CustomerManagement.last)
  end

  test "should show customer_management" do
    get customer_management_url(@customer_management)
    assert_response :success
  end

  test "should get edit" do
    get edit_customer_management_url(@customer_management)
    assert_response :success
  end

  test "should update customer_management" do
    patch customer_management_url(@customer_management), params: { customer_management: { amount_paid: @customer_management.amount_paid, customer: @customer_management.customer } }
    assert_redirected_to customer_management_url(@customer_management)
  end

  test "should destroy customer_management" do
    assert_difference('CustomerManagement.count', -1) do
      delete customer_management_url(@customer_management)
    end

    assert_redirected_to customer_managements_url
  end
end
