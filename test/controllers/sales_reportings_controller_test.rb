require 'test_helper'

class SalesReportingsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @sales_reporting = sales_reportings(:one)
  end

  test "should get index" do
    get sales_reportings_url
    assert_response :success
  end

  test "should get new" do
    get new_sales_reporting_url
    assert_response :success
  end

  test "should create sales_reporting" do
    assert_difference('SalesReporting.count') do
      post sales_reportings_url, params: { sales_reporting: { amount_paid: @sales_reporting.amount_paid, product: @sales_reporting.product, quantity: @sales_reporting.quantity } }
    end

    assert_redirected_to sales_reporting_url(SalesReporting.last)
  end

  test "should show sales_reporting" do
    get sales_reporting_url(@sales_reporting)
    assert_response :success
  end

  test "should get edit" do
    get edit_sales_reporting_url(@sales_reporting)
    assert_response :success
  end

  test "should update sales_reporting" do
    patch sales_reporting_url(@sales_reporting), params: { sales_reporting: { amount_paid: @sales_reporting.amount_paid, product: @sales_reporting.product, quantity: @sales_reporting.quantity } }
    assert_redirected_to sales_reporting_url(@sales_reporting)
  end

  test "should destroy sales_reporting" do
    assert_difference('SalesReporting.count', -1) do
      delete sales_reporting_url(@sales_reporting)
    end

    assert_redirected_to sales_reportings_url
  end
end
