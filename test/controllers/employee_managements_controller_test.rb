require 'test_helper'

class EmployeeManagementsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @employee_management = employee_managements(:one)
  end

  test "should get index" do
    get employee_managements_url
    assert_response :success
  end

  test "should get new" do
    get new_employee_management_url
    assert_response :success
  end

  test "should create employee_management" do
    assert_difference('EmployeeManagement.count') do
      post employee_managements_url, params: { employee_management: { clockin: @employee_management.clockin, clockout: @employee_management.clockout, employee: @employee_management.employee } }
    end

    assert_redirected_to employee_management_url(EmployeeManagement.last)
  end

  test "should show employee_management" do
    get employee_management_url(@employee_management)
    assert_response :success
  end

  test "should get edit" do
    get edit_employee_management_url(@employee_management)
    assert_response :success
  end

  test "should update employee_management" do
    patch employee_management_url(@employee_management), params: { employee_management: { clockin: @employee_management.clockin, clockout: @employee_management.clockout, employee: @employee_management.employee } }
    assert_redirected_to employee_management_url(@employee_management)
  end

  test "should destroy employee_management" do
    assert_difference('EmployeeManagement.count', -1) do
      delete employee_management_url(@employee_management)
    end

    assert_redirected_to employee_managements_url
  end
end
